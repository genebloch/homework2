﻿using System;
using System.Threading.Tasks;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main()
        {
            Wrapper rabbitMQ = new Wrapper("PingPong");

            Task.Run(() =>
            {
                rabbitMQ.ListenQueue("pong_queue");
            });

            Console.WriteLine("Press ESC to stop");
            do
            {
                while (!Console.KeyAvailable)
                {
                    rabbitMQ.SendMessageToQueue("ping_queue", $"Ping: {DateTime.Now}");
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            rabbitMQ.Dispose();
        }
    }
}
