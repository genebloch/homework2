﻿using System;
using System.Threading.Tasks;
using RabbitMQ.Wrapper;

namespace Ponger
{ 
    class Program
    {
        static void Main()
        {
            Wrapper rabbitMQ = new Wrapper("PingPong");

            Task.Run(() =>
            {
                rabbitMQ.ListenQueue("ping_queue");
            });

            Console.WriteLine("Press ESC to stop");
            do
            {
                while (!Console.KeyAvailable)
                {
                    rabbitMQ.SendMessageToQueue("pong_queue", $"Pong: {DateTime.Now}");
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            rabbitMQ.Dispose();
        }
    }
}
