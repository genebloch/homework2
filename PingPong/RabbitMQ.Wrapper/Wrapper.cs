﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper
{
    public class Wrapper : IDisposable
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _exchangeName;

        public Wrapper(string exchangeName)
        {
            _connectionFactory = new ConnectionFactory() { HostName = "localhost" };
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();

            _exchangeName = exchangeName;
        }

        public void SendMessageToQueue(string queueName, string message)
        {
            _channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: _exchangeName,
                                 routingKey: queueName,
                                 basicProperties: null,
                                 body: body);

            Console.WriteLine($"Sent: \"{message}\"");

            Thread.Sleep(2500);
        }

        public void ListenQueue(string queueName)
        {
            _channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct);

            _channel.QueueDeclare(queue: queueName,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false);

            _channel.QueueBind(queueName, _exchangeName, queueName);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += MessageReceived;

            _channel.BasicConsume(queue: queueName,
                                 autoAck: false,
                                 consumer: consumer);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            Console.WriteLine($"Received: \"{message}\"");

            _channel.BasicAck(args.DeliveryTag, false);
        }
    }
}
